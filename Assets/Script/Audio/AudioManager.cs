﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Singleton<AudioManager>
{
    public Sound[] gameSounds;

    [Space]

    public float mainClipStartTime = 0;
    public float mainClipEndTime = 0;

    private Sound mainClip;
    private IEnumerator mainClipCoroutine;

    private void Awake()
    {
        InitializeClips();
    }

    private void InitializeClips()
    {
        foreach (var sound in gameSounds)
        {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;
            sound.source.volume = sound.volume;
            if(sound.name == "MainClip")
            {
                mainClip = sound;
            }
        }
    }

    public void PlayClip(string clipName)
    {
        Sound sound = null;
        
        for (int i = 0; i < gameSounds.Length; i++)
        {
            if(gameSounds[i].name == clipName)
            {
                sound = gameSounds[i];
                break;
            }
        }

        if(sound == null)
        {
            Debug.LogError($"Couldn't find sound called: {clipName}");
            return;
        }

        sound.source.Play();
    }

    public void PlayMainClip()
    {
        mainClip.source.Play();
        mainClip.source.time = 20;
        mainClipCoroutine = MainClipOnEnd(mainClipEndTime - mainClipStartTime);
        StartCoroutine(mainClipCoroutine);
    }

    public void ResumeMainClip()
    {
        mainClip.source.UnPause();
        mainClipCoroutine = MainClipOnEnd(mainClipEndTime - mainClip.source.time);
        StartCoroutine(mainClipCoroutine);
    }

    public void PauseMainClip()
    {
        mainClip.source.Pause();
        if (mainClipCoroutine != null) 
        {
            StopCoroutine(mainClipCoroutine);          
        }
    }

    public void StopMainClip()
    {
        mainClip.source.Stop();
    }

    private IEnumerator MainClipOnEnd(float timeLeft)
    {
        yield return new WaitForSeconds(timeLeft);
        StopMainClip();
        OnMainClipFinished();
    }

    public static event System.Action mainClipFinished;
    private static void OnMainClipFinished()
    {
        mainClipFinished?.Invoke();
    }
}
