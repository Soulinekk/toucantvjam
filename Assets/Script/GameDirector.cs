﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewind;

public class GameDirector : MonoBehaviour
{
    public RemoteController remote;
    public GameplayAnimationController animationController;
    private GameplayState gameplayState = GameplayState.Beggining;

    private void Start()
    {
        RemoteController.button01Pressed += OnPlayButtonClick;
        RemoteController.button02Pressed += OnStopButtonClick;
        RewindMaster.RewindFinished += OnRewindFinished;
        RewindMaster.RecordingFinished += OnRecordingFinished;
    }

    private void OnPlayButtonClick(bool wasClicked)
    {
        switch (gameplayState)
        {
            case GameplayState.Beggining:
                animationController.StartAnimation();
                AudioManager.Instance.PlayMainClip();
                gameplayState = GameplayState.Playing;
                break;

            case GameplayState.StoppedRecording:
                RewindMaster.RewindAll();
                gameplayState = GameplayState.Rewinding;
                break;
        }
    }

    private void OnStopButtonClick(bool wasClicked)
    {
        if(gameplayState == GameplayState.Playing)
        {
            animationController.StopAnimator();
            AudioManager.Instance.PauseMainClip();
            RewindMaster.StartRecordingAll();
            gameplayState = GameplayState.Recording;
        }
    }

    private void OnRecordingFinished()
    {
        gameplayState = GameplayState.StoppedRecording;
    }

    private void OnRewindFinished()
    {
        animationController.ResumeAnimator();
        AudioManager.Instance.PlayMainClip();
        gameplayState = GameplayState.Playing;
    }

    //Forgive me this bestiality, tiredness striks me
    public enum GameplayState
    {
        Beggining = 0,
        Playing = 1,
        Recording = 2,
        StoppedRecording = 3,
        Rewinding = 4,
    }
}
