﻿public enum HumanoidAnimationState
{
    Stopped = 0,
    Idle = 1,
    Awoken = 2,
    Loving = 3,
    HeadBanging = 4
}
