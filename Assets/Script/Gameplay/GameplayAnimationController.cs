﻿using UnityEngine;

public class GameplayAnimationController : MonoBehaviour
{
    private const string triggerAwakeName = "Awake";
    private const string triggerBeginHeadBangingName = "BeginHeadBanging";
    private const string triggerMakeLoveName = "MakeLove";
    private const string triggerFinishLovingName = "FinishLoving";
    private const string triggerGoBackToHeadBangingName = "GoBackToHeadBanging";

    public Animator[] objectsAnimator;

    private HumanoidAnimationState animationState = HumanoidAnimationState.Idle;
    
    public void InitializeObjects()
    {
        SetAnimatorsTrigger(triggerAwakeName);
        animationState = HumanoidAnimationState.Awoken;
    }

    public void PlayNextAnimStep()
    {
        switch (animationState)
        {
            case HumanoidAnimationState.Stopped:
                SetAnimatorsTrigger(triggerGoBackToHeadBangingName);
                animationState = HumanoidAnimationState.HeadBanging;
                break;

            case HumanoidAnimationState.Idle:
                SetAnimatorsTrigger(triggerAwakeName);
                animationState = HumanoidAnimationState.Awoken;
                break;

            case HumanoidAnimationState.Awoken:
                SetAnimatorsTrigger(triggerMakeLoveName);
                animationState = HumanoidAnimationState.Loving;
                break;

            case HumanoidAnimationState.Loving:
                SetAnimatorsTrigger(triggerFinishLovingName);
                animationState = HumanoidAnimationState.HeadBanging;
                break;
        }
    }

    // Szpachla, couldn't make it in time
    public void StartAnimation()
    {
        SetAnimatorsTrigger(triggerAwakeName);
        animationState = HumanoidAnimationState.HeadBanging;
    }

    public void ResumeAnimator()
    {
        foreach (var animator in objectsAnimator)
        {
            animator.enabled = true;
            animator.speed = 1;
        }
        animationState = HumanoidAnimationState.HeadBanging;
    }

    public void StopAnimator()
    {
        foreach (var animator in objectsAnimator)
        {
            animator.speed = 0;
            animator.enabled = false;
        }
        animationState = HumanoidAnimationState.Stopped;
    }

    public void GoBackToHeadBanging()
    {
        foreach (var animator in objectsAnimator)
        {
            animator.StartPlayback();
        }
        animationState = HumanoidAnimationState.HeadBanging;
    }

    private void SetAnimatorsTrigger(string triggerKey)
    {
        foreach (var animator in objectsAnimator)
        {
            animator.SetTrigger(triggerKey);
        }
    }
}
