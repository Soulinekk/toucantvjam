﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ESCToQuit : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Its way to late to think about something better :D
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
