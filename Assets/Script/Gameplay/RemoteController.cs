﻿using System.Collections;
using UnityEngine;

public class RemoteController : MonoBehaviour
{
    public Transform button01Transform;
    public Transform button02Transform;
    public Transform button03Transform;

    [Space]

    public float pressedDuration = 0.15f;
    public float buttonStartHeight;
    public float buttonEndHeight;

    [Space]

    public AudioClip audioOnClick;

    private bool button01WasPressed = false;
    private bool button02WasPressed = false;
    private bool button03WasPressed = false;

    private IEnumerator button01Release;
    private IEnumerator button02Release;
    private IEnumerator button03Release;

    // Start is called before the first frame update
    void Awake()
    {
        button01Pressed += Press01Button;
        button02Pressed += Press02Button;
        button03Pressed += Press03Button;
        button01Transform.localPosition = new Vector3(button01Transform.localPosition.x, buttonStartHeight, button01Transform.localPosition.z);
        button02Transform.localPosition = new Vector3(button02Transform.localPosition.x, buttonStartHeight, button02Transform.localPosition.z);
        button03Transform.localPosition = new Vector3(button03Transform.localPosition.x, buttonStartHeight, button03Transform.localPosition.z);
    }

    // Update is called once per frame
    void Update()
    {
        MonitorInput();
    }

    private void MonitorInput()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
        {
            OnButton01Pressed(button01WasPressed);
        }

        if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
        {
            OnButton02Pressed(button02WasPressed);
        }

        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
        {
            OnButton03Pressed(button03WasPressed);
        }
    }

    private void Press01Button(bool wasPressedPreviously)
    {
        PressButton(1);
    }
    private void Press02Button(bool wasPressedPreviously)
    {
        PressButton(2);
    }
    private void Press03Button(bool wasPressedPreviously)
    {
        PressButton(3);
    }

    private void PressButton(int buttonIndex)
    {
        AudioManager.Instance.PlayClip("ButtonClick");
        if (buttonIndex == 1)
        {
            if(button01Release != null)
            {
                StopCoroutine(button01Release);
            }
            button01Release = ReleaseButtonCoroutine(1);
            StartCoroutine(button01Release);

            button01Transform.localPosition = new Vector3(button01Transform.localPosition.x, buttonEndHeight, button01Transform.localPosition.z);
            button01WasPressed = true;
        }
        else if (buttonIndex == 2)
        {
            if (button02Release != null)
            {
                StopCoroutine(button02Release);
            }
            button02Release = ReleaseButtonCoroutine(2);
            StartCoroutine(button02Release);

            button02Transform.localPosition = new Vector3(button02Transform.localPosition.x, buttonEndHeight, button02Transform.localPosition.z);
            button02WasPressed = true;
        }
        else if (buttonIndex == 3)
        {
            if (button03Release != null)
            {
                StopCoroutine(button03Release);
            }
            button03Release = ReleaseButtonCoroutine(3);
            StartCoroutine(button03Release);

            button03Transform.localPosition = new Vector3(button03Transform.localPosition.x, buttonEndHeight, button03Transform.localPosition.z);
            button03WasPressed = true;
        }
    }

    private IEnumerator ReleaseButtonCoroutine(int buttonIndex)
    {
        yield return new WaitForSeconds(pressedDuration);

        if(buttonIndex == 1)
        {
            button01Transform.localPosition = new Vector3(button01Transform.localPosition.x, buttonStartHeight, button01Transform.localPosition.z);
            button01WasPressed = false;
        }
        else if(buttonIndex == 2)
        {
            button02Transform.localPosition = new Vector3(button02Transform.localPosition.x, buttonStartHeight, button02Transform.localPosition.z);
            button02WasPressed = false;
        }
        else if(buttonIndex == 3)
        {
            button03Transform.localPosition = new Vector3(button03Transform.localPosition.x, buttonStartHeight, button03Transform.localPosition.z);
            button03WasPressed = false;
        }
    }

    private void OnDestroy()
    {
        button01Pressed -= Press01Button;
        button02Pressed -= Press02Button;
        button03Pressed -= Press03Button;
    }

    /// <summary>
    /// Bool tells if button was already pressed when user pressed it again.
    /// </summary>
    public static event System.Action<bool> button01Pressed;
    private void OnButton01Pressed(bool wasPressedPreviously)
    {
        button01Pressed?.Invoke(wasPressedPreviously);
    }

    /// <summary>
    /// Bool tells if button was already pressed when user pressed it again.
    /// </summary>
    public static event System.Action<bool> button02Pressed;
    private void OnButton02Pressed(bool wasPressedPreviously)
    {
        button02Pressed?.Invoke(wasPressedPreviously);
    }

    /// <summary>
    /// Bool tells if button was already pressed when user pressed it again.
    /// </summary>
    public static event System.Action<bool> button03Pressed;
    private void OnButton03Pressed(bool wasPressedPreviously)
    {
        button03Pressed?.Invoke(wasPressedPreviously);
    }

}
