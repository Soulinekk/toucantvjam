﻿using System;
using System.Collections;
using UnityEngine;

public class MonoBehaviourHost : Singleton<MonoBehaviour>
{
    private void Update()
    {
        if(UpdateHost != null)
        {
            UpdateHost();
        }
    }

    private void FixedUpdate()
    {
        if (FixedUpdateHost != null)
        {
            FixedUpdateHost();
        }
    }

    private void LateUpdate()
    {
        if (LateUpdateHost != null)
        {
            LateUpdateHost();
        }
    }

    public static void StartCoroutineHost(IEnumerator coroutine)
    {
        Instance.StartCoroutine(coroutine);
    } 

    public static void StopCoroutineHost(IEnumerator coroutine)
    {
        Instance.StopCoroutine(coroutine);
    }

    public static event Action UpdateHost;
    public static event Action FixedUpdateHost;
    public static event Action LateUpdateHost;
}
