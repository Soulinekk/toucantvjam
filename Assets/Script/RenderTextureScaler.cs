﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderTextureScaler : MonoBehaviour
{
    public Camera targetCamera;
    public Material targetMaterial;
    private int lastWidth;
    private int lastHeight;
    private IEnumerator resolutionCheckCoroutine;

    // Start is called before the first frame update
    void Start()
    {
        CreateRenderTexture();
        BeginResolutionCheckCoroutine();
    }

    [ContextMenu("CreateRenderTexture")]
    void CreateRenderTexture()
    {
        if(targetCamera.targetTexture != null)
        {
            targetCamera.targetTexture.Release();
        }
        lastWidth = Screen.width;
        lastHeight = Screen.height;
        targetCamera.targetTexture = new RenderTexture(lastWidth, lastHeight, 24);
        targetMaterial.mainTexture = targetCamera.targetTexture;
    }

    void BeginResolutionCheckCoroutine()
    {
        if(resolutionCheckCoroutine != null)
        {
            StopCoroutine(resolutionCheckCoroutine);
        }
        resolutionCheckCoroutine = CheckingForResolutionChange();
        StartCoroutine(resolutionCheckCoroutine);
    }

    IEnumerator CheckingForResolutionChange()
    {
        while (0 == 0)
        {
            if(lastWidth != Screen.width || lastHeight != Screen.height)
            {
                CreateRenderTexture();
            }

            yield return new WaitForSeconds(3f);
        }
    }
}
