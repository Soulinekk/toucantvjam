﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewind;

public class ObjectRecorder : Recorder<RewindTickData>
{
    public override void RecordFrame()
    {
        recordedTicks.Add(new RewindTickData(transform.localPosition, transform.localRotation, Time.time));
    }

    public override void Rewind(float rewindDurration)
    {
        rewindStart = Time.time;
        MonoBehaviourHost.UpdateHost += RewindLerp;
    }

    public override void RewindTick(float time)
    {
        throw new System.NotImplementedException();
    }

    // It should work like this, it should use all recorded ticks, but Im too tired right now to implement this
    protected void RewindLerp()
    {
        float progress = RewindMaster.rewindDuration - (RewindMaster.rewindDuration - (Time.time - rewindStart));
        Mathf.Clamp01(progress);
        transform.localPosition = Vector3.Slerp(recordedTicks[recordedTicks.Count - 1].position, recordedTicks[0].position, progress);
        transform.localRotation = Quaternion.Slerp(recordedTicks[recordedTicks.Count - 1].rotation, recordedTicks[0].rotation, progress); 

        if(progress >= 1)
        {
            MonoBehaviourHost.UpdateHost -= RewindLerp;
            RewindMaster.OnRewindFinished();
        }
    }
}