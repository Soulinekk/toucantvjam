﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rewind
{
    public interface IRecorder
    {
        void StartRecording();
        void StopRecording();
        void Rewind(float rewindDurration);
        void RecordFrame();
        void RewindTick(float time);
    }

}