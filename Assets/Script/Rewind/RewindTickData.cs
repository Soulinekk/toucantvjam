﻿using UnityEngine;

namespace Rewind
{
    public class RewindTickData
    {
        public Vector3 position;
        public Quaternion rotation;
        public float time;

        public RewindTickData()
        {
            position = Vector3.zero;
            rotation = Quaternion.identity;
            time = 0;
        }

        public RewindTickData(Vector3 position, Quaternion rotation, float time)
        {
            this.position = position;
            this.rotation = rotation;
            this.time = time;
        }
    }
}