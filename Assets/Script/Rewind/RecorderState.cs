﻿namespace Rewind
{
    public enum RecorderState
    {
        Uninitialized = 0,
        Idle = 1,
        Recording = 2,
        Stopped = 3
    }
}