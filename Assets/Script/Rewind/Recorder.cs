﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rewind
{
    public abstract class Recorder<T> : MonoBehaviour, IRecorder
    {
        public List<T> recordedTicks = new List<T>();
        public RecorderState recorderState = RecorderState.Uninitialized;

        protected float rewindStart;
        protected float rewindLastTickTime;

        private Rigidbody rigidbody;
        
        protected void Awake()
        {
            Initialize();
            rigidbody = GetComponent<Rigidbody>();
        }

        public abstract void RecordFrame();

        public abstract void RewindTick(float time);

        protected void Initialize()
        {
            RegisterRecorder();
            recorderState = RecorderState.Idle;
        }

        protected void RegisterRecorder()
        {
            RewindMaster.recorders.Add(this);
        }

        protected void UnregisterRecorder()
        {
            RewindMaster.recorders.Remove(this);
        }

        public void StartRecording()
        {
            MonoBehaviourHost.UpdateHost += RecordFrame;
            rigidbody.isKinematic = false;
            recorderState = RecorderState.Recording;
        }

        public void StopRecording()
        {
            MonoBehaviourHost.UpdateHost -= RecordFrame;
            rigidbody.isKinematic = true;
            recorderState = RecorderState.Stopped;
        }

        public abstract void Rewind(float rewindDurration);

        protected void OnDestroy()
        {
            MonoBehaviourHost.UpdateHost -= RecordFrame;
            UnregisterRecorder();
        }
    }
}