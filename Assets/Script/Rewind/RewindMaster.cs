﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rewind
{
    public static class RewindMaster
    {
        public static List<IRecorder> recorders = new List<IRecorder>();
        public static float tickInterval = 0.02f;
        public static float rewindDuration = 2.5f;
        private static IEnumerator delayedStopRecording;

        public static void StartRecordingAll()
        {
            for (int i = 0; i < recorders.Count; i++)
            {
                recorders[i].StartRecording();
            }
            if(delayedStopRecording != null)
            {
                MonoBehaviourHost.StopCoroutineHost(delayedStopRecording);
            }
            delayedStopRecording = StopRecordingDelayed();
            MonoBehaviourHost.StartCoroutineHost(delayedStopRecording);
        }

        public static void StopRecordingAll()
        {
            if (delayedStopRecording != null)
            {
                MonoBehaviourHost.StopCoroutineHost(delayedStopRecording);
            }

            for (int i = 0; i < recorders.Count; i++)
            {
                recorders[i].StopRecording();
            }
            OnRecordingFinished();
        }

        public static void RewindAll()
        {
            StopRecordingAll();

            for (int i = 0; i < recorders.Count; i++)
            {
                recorders[i].Rewind(rewindDuration);
            }
            MonoBehaviourHost.StartCoroutineHost(OnRewindFinished());
        }

        private static IEnumerator StopRecordingDelayed()
        {
            yield return new WaitForSeconds(rewindDuration);

            StopRecordingAll();
        }

        public static event System.Action RecordingFinished;
        static void OnRecordingFinished()
        {
            RecordingFinished?.Invoke();
        }

        public static event System.Action RewindFinished;
        public static IEnumerator OnRewindFinished()
        {
            yield return new WaitForSeconds(rewindDuration);
            RewindFinished?.Invoke();
        }
    }
}
